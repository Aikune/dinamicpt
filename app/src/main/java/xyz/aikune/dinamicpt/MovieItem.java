package xyz.aikune.dinamicpt;

public class MovieItem {
    private String image;
    private String title;
    private String description;
    private int id;

    public MovieItem(String image, String title, String description) {
        super();
        this.image = image;
        this.title = title;
        this.description = description;
    }

    public MovieItem (String image, int id){
        super();
        this.image = image;
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }
}
