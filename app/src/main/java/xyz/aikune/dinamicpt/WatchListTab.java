package xyz.aikune.dinamicpt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WatchListTab extends Fragment {
    private ListView listView;
    private static final String API_URL = "http://api.themoviedb.org/3/";
    private static final String API_KEY = "91918ec376a21251b4dc12d57c24129b";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_watchlist, container, false);
        listView =(ListView) view.findViewById(R.id.listViewWL);
        if (!isOnline()){
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
            String watchlist = sharedPreferences.getString("watchlist", "");
            if (!watchlist.equals("")){
                List <MovieItem>items = new ArrayList<>();
                listView.setAdapter(new ItemAdapter(this.getActivity(),items));
                try {
                    JSONObject jsonResponse = (JSONObject) new JSONTokener(watchlist).nextValue();
                    JSONArray jsonArray = jsonResponse.getJSONArray("results");
                    for (int i=0;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        String image = "https://image.tmdb.org/t/p/w185/" + jsonObject.getString("poster_path");
                        String title = jsonObject.getString("title");
                        String description = jsonObject.getString("overview");
                        items.add(new MovieItem(image,title,description));
                    }
                } catch (JSONException e) {
                    Log.e("ERROR", e.getMessage(), e);
                }
            }
        }
        else
            new QueryAPI().execute(this.getActivity());
        return view;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    class QueryAPI extends AsyncTask<Context, Void, String> {
        private Context context;
        protected void onPreExecute() {
        }

        protected String doInBackground(Context... urls) {
            this.context = urls[0];
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
            String user_id = sharedPreferences.getString("user_id", "");
            String session_id = sharedPreferences.getString("session_id","");
            try {
                URL url = new URL(API_URL + "account/" + user_id + "/watchlist/movies"+ "?api_key=" + API_KEY +
                        "&session_id=" + session_id);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            Log.i("INFO", response);
            List <MovieItem>items = new ArrayList<>();
            listView.setAdapter(new ItemAdapter(this.context,items));
            try {
                JSONObject jsonResponse = (JSONObject) new JSONTokener(response).nextValue();
                JSONArray jsonArray = jsonResponse.getJSONArray("results");
                for (int i=0;i<jsonArray.length();i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String image = "https://image.tmdb.org/t/p/w185/" + jsonObject.getString("poster_path");
                    String title = jsonObject.getString("title");
                    String description = jsonObject.getString("overview");
                    items.add(new MovieItem(image,title,description));
                }
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("watchlist", response);
                editor.apply();
            } catch (JSONException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            catch (ClassCastException e){
                Log.d("DEBUG", "Sesion invalida");
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("session_id", "");
                editor.putString("user_id", "");
                editor.putString("user_name","");
                editor.apply();
                Intent intent = new Intent (getActivity(),MainActivity.class);
                getActivity().finish();
                startActivity(intent);
                getActivity().overridePendingTransition(0, 0);
            }
        }
    }
}
