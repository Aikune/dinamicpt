package xyz.aikune.dinamicpt;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PopularTab extends Fragment {
    private ViewPager viewPager;
    private static final String API_URL = "http://api.themoviedb.org/3/";
    private static final String API_KEY = "91918ec376a21251b4dc12d57c24129b";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_popular, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.pagerPopular);
        if (isOnline()){
            new QueryAPI().execute();
        }else{
            Toast.makeText(getActivity().getApplicationContext(), getString(R.string.internet2), Toast.LENGTH_LONG).show();
        }
        return view;
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    class QueryAPI extends AsyncTask<Void, Void, String> {
        protected void onPreExecute() {
        }

        protected String doInBackground(Void... urls) {
            try {
                URL url = new URL(API_URL + "movie/popular"+ "?api_key=" + API_KEY + "&language=es");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    return stringBuilder.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String response) {
            if (response == null) {
                response = "THERE WAS AN ERROR";
            }
            viewPager.setAdapter(new CustomPagerAdapter(getActivity()));
            viewPager.setOnTouchListener(new View.OnTouchListener() {
                private float pointX;
                private float pointY;
                private int tolerance = 50;

                @Override
                public boolean onTouch(View view, MotionEvent event) {
                    switch (event.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            return false;
                        case MotionEvent.ACTION_DOWN:
                            pointX = event.getX();
                            pointY = event.getY();
                            break;
                        case MotionEvent.ACTION_UP:
                            boolean sameX = pointX + tolerance > event.getX() && pointX - tolerance < event.getX();
                            boolean sameY = pointY + tolerance > event.getY() && pointY - tolerance < event.getY();
                            if (sameX && sameY) {
                                Toast.makeText(getActivity().getApplicationContext(), getString(R.string.added), Toast.LENGTH_LONG).show();
                                CustomPagerAdapter adapter = (CustomPagerAdapter) viewPager.getAdapter();
                                int id = adapter.getItems().get(viewPager.getCurrentItem()).getId();
                                new PostAPI().execute(id);
                            }
                    }
                    return false;
                }
            });
            try {
                JSONObject jsonResponse = (JSONObject) new JSONTokener(response).nextValue();
                JSONArray jsonArray = jsonResponse.getJSONArray("results");
                for (int i=0;i<jsonArray.length();i++)
                {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    String image = "https://image.tmdb.org/t/p/w185/" + jsonObject.getString("poster_path");
                    int id = jsonObject.getInt("id");
                    CustomPagerAdapter adapter =(CustomPagerAdapter)viewPager.getAdapter();
                    adapter.getItems().add(new MovieItem(image, id));
                    viewPager.getAdapter().notifyDataSetChanged();

                }
            } catch (JSONException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            catch (ClassCastException e){
                Log.d("DEBUG", "Sesion invalida");
                SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("session_id", "");
                editor.putString("user_id", "");
                editor.putString("user_name","");
                editor.apply();
                Intent intent = new Intent (getActivity(),MainActivity.class);
                getActivity().finish();
                startActivity(intent);
                getActivity().overridePendingTransition(0,0);
            }
        }
    }

    class CustomPagerAdapter extends PagerAdapter {
        Context context;
        LayoutInflater inflater;
        private List<MovieItem> items= new ArrayList<>();

        public CustomPagerAdapter(Context context) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public List<MovieItem> getItems() {
            return items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == (object);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View itemView = inflater.inflate(R.layout.pager_item, container, false);

            ImageView imageView = (ImageView) itemView.findViewById(R.id.imageViewPopular);
            Picasso.with(context).load(items.get(position).getImage()).into(imageView);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            notifyDataSetChanged();
            container.removeView((LinearLayout) object);
        }
    }

    class PostAPI extends AsyncTask<Integer, Void, String>{

        @Override
        protected String doInBackground(Integer... params) {
            SharedPreferences sharedPreferences = getActivity().getSharedPreferences("preferences", Context.MODE_PRIVATE);
            String session_id = sharedPreferences.getString("session_id", "");
            String user_id = sharedPreferences.getString("user_id", "");
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("media_type", "movie");
                jsonObject.put("media_id", params[0].intValue());
                jsonObject.put("watchlist", true);
            } catch (JSONException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            try {
                URL url = new URL(API_URL + "account/" + user_id + "/watchlist"+ "?api_key=" + API_KEY + "&session_id=" + session_id);
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestProperty("Content-Type", "application/json");
                httpURLConnection.setRequestProperty("Accept", "application/json");
                httpURLConnection.setRequestMethod("POST");
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
                outputStreamWriter.write(jsonObject.toString());
                outputStreamWriter.flush();
                StringBuilder sb = new StringBuilder();
                int HttpResult = httpURLConnection.getResponseCode();
                if(HttpResult == HttpURLConnection.HTTP_OK){
                    BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream(),"utf-8"));
                    String line = null;
                    while ((line = br.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    br.close();
                    Log.i("ERROR",sb.toString());
                }else{
                    Log.i("ERROR",httpURLConnection.getResponseMessage());
                }
            } catch (Exception e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            return null;
        }
    }
}
