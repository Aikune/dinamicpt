package xyz.aikune.dinamicpt;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTabHost;
import android.widget.TextView;


public class UserActivity extends FragmentActivity {
    private FragmentTabHost tabHost;
    private TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        textView = (TextView) findViewById(R.id.textViewUser);
        tabHost = (FragmentTabHost) findViewById(R.id.tabHost);
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String user_name = sharedPreferences.getString("user_name", "");
        if (!user_name.equals("")){
            textView.setText(user_name);
        }
        tabHost.setup(this,getSupportFragmentManager(),android.R.id.tabcontent);
        tabHost.addTab(tabHost.newTabSpec("tab1").setIndicator(getString(R.string.favorites)), FavoritesTab.class, null);
        tabHost.addTab(tabHost.newTabSpec("tab2").setIndicator(getString(R.string.populars)),PopularTab.class,null);
        tabHost.addTab(tabHost.newTabSpec("tab3").setIndicator(getString(R.string.watchlist)),WatchListTab.class,null);
    }
}
