package xyz.aikune.dinamicpt;

import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;

public class ItemAdapter extends BaseAdapter {
    private Context context;
    private List<MovieItem> items;

    public ItemAdapter(Context context, List<MovieItem> items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.item_movie, parent, false);
        }
        ImageView imageView = (ImageView) rowView.findViewById(R.id.poster);
        TextView textView = (TextView) rowView.findViewById(R.id.title);
        TextView textViewD = (TextView) rowView.findViewById(R.id.description);
        MovieItem item = this.items.get(position);
        String url = item.getImage();
        Picasso.with(context).load(url).into(imageView);
        textView.setText(item.getTitle());
        textViewD.setText(item.getDescription());
        return rowView;
    }


}
