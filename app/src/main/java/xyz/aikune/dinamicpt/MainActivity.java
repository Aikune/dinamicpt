package xyz.aikune.dinamicpt;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends Activity implements DialogInterface.OnDismissListener {
    private static final String API_URL = "http://api.themoviedb.org/3/";
    private static final String API_KEY = "91918ec376a21251b4dc12d57c24129b";
    private String session_id;
    private String user_id;
    private String user_name;
    private String response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        session_id = sharedPreferences.getString("session_id","");
        user_id = sharedPreferences.getString("user_id","");
        user_name = sharedPreferences.getString("user_name","");
        if (isOnline()){
            if (!session_id.equals("")){
                Intent intent = new Intent (this,UserActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(0,0);
            } else{
                doAuth();
            }
        } else{
            if (!session_id.equals("")) {
                Intent intent = new Intent(this, UserActivity.class);
                finish();
                startActivity(intent);
                overridePendingTransition(0,0);
            } else {
                Context context = getApplicationContext();
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, getString(R.string.internet), duration);
                toast.show();
                finish();
            }
        }

    }

    public boolean isOnline(){
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void doAuth(){
        FragmentManager fragmentManager = getFragmentManager();
        AuthFragment authFragment = new AuthFragment();
        authFragment.show(fragmentManager, "");
    }

    @Override
    public void onDismiss(DialogInterface dialog){
        doLogin();
        Intent intent = new Intent(this, UserActivity.class);
        finish();
        startActivity(intent);
        overridePendingTransition(0,0);
    }

    public void doLogin(){
        SharedPreferences sharedPreferences = getSharedPreferences("preferences", Context.MODE_PRIVATE);
        String token = sharedPreferences.getString("token","");
        String param = API_URL + "authentication/session/new" + "?api_key=" + API_KEY +
                "&request_token=" + token;
        response="";
        new QueryAPI().execute(param);
        while(response.equals("") && !response.equals("error")) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
        }
        if (response.equals("error")){
            finish();
            startActivity(getIntent());
        }else {
            try {
                JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                session_id = jsonObject.getString("session_id");
            } catch (JSONException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            param = API_URL + "account" + "?api_key=" + API_KEY + "&session_id=" + session_id;
            response = "";
            new QueryAPI().execute(param);
            while (response.equals("")) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    Log.e("ERROR", e.getMessage(), e);
                }
            }
            try {
                JSONObject jsonObject = (JSONObject) new JSONTokener(response).nextValue();
                user_id = jsonObject.getString("id");
                user_name = jsonObject.getString("username");
            } catch (JSONException e) {
                Log.e("ERROR", e.getMessage(), e);
            }
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("session_id", session_id);
            editor.putString("user_id", user_id);
            editor.putString("user_name", user_name);
            editor.apply();
        }
    }

    class QueryAPI extends AsyncTask<String, Void, String> {
        protected void onPreExecute() {
        }

        protected String doInBackground(String... urls) {
            try {
                URL url = new URL(urls[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                try {
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader
                            (urlConnection.getInputStream()));
                    StringBuilder stringBuilder = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line).append("\n");
                    }
                    bufferedReader.close();
                    response=stringBuilder.toString();
                    return stringBuilder.toString();
                }
                    finally {
                    urlConnection.disconnect();
                }

            } catch (Exception e) {
                response = "error";
                Log.e("ERROR", e.getMessage(), e);
                return null;
            }
        }

        protected void onPostExecute(String resp) {

        }
    }


}
